
From kadian@nurf.com Fri Mar 20 13:03:25 1998
Date: Fri, 20 Mar 1998 10:55:16 -0500 (EST)
From: Will <kadian@nurf.com>
To: men@algonet.se, kermit@oz.net, darkoth@rep.mudservices.com
Subject: Otype Code by Kadian

I sent this to the three of you because you either have an outdated
version or don't know what it it or just don't have it :)

Will


This function lists a type of weapon or item to the screen so
you don't need to go to a file and check it and you can check items
seperately... for example: otype weapon axe :would show all axes in all
the areas ...this way you could see which weapon types don't have enough
or have too many or you could check out items you never heard
of...anyhow...I think it's great....

otype <type> <wear/weapon>

e.g. otype light

     otype weapon axe

     otype armor hands





to add in the code quite simple...add in what you need in

interp.c and interp.h and help.are





then add the below into act_wiz.c (you may need to make saome minor
changes

if you do not have olc installed)





/* This code was written by Kadian of Distant Thunder if you have any
problems

with this code feel free to e-mail at kadian@nurf.com ...no need to give

any credit...it's a tiny snippet :) */





void do_otype(CHAR_DATA *ch, char *argument)

{

    int type;

    int type2;

    int vnum=1;

    char buf[MAX_STRING_LENGTH];

    char buffer[12 * MAX_STRING_LENGTH];

    char arg1[MAX_INPUT_LENGTH];

    char arg2[MAX_INPUT_LENGTH];

    char *item;      

    OBJ_INDEX_DATA *obj;

    bool found;





    item = one_argument(argument, arg1);

    one_argument ( item , arg2);





    found = FALSE;

    buffer [0] = '\0';





    if (arg1[0] == '\0')     

        {

        send_to_char("Type 'Help Otype' for usage\n\r",ch);

        return;

        }





    type2 = 0;





    if ((!str_cmp(arg1,"armor") || !str_cmp(arg1,"weapon"))

        && arg2[0] == NULL)

        {        

        send_to_char("Type 'Help Otype' for proper usage.\n\r",ch);

        return;

        }

    else if (!str_cmp(arg1,"armor"))

        {

        type = flag_value(type_flags,arg1);

        if ((type2 = flag_value(wear_flags,arg2)) == NO_FLAG)

          {

                send_to_char("No such armor type.\n\r",ch);

                return;      

          }

        }

    else if (!str_cmp(arg1,"weapon"))

        {

        type = flag_value(type_flags,arg1);

        if ((type2 = flag_value(weapon_class,arg2)) == NO_FLAG)

          {

                send_to_char("No such weapon type.\n\r",ch);

                return;

          }            

        }

    else

        {

            if((type = flag_value(type_flags,arg1)) == NO_FLAG)

                {

                send_to_char("Unknown Type.\n\r", ch);

                return;

                }

        }        





    for(;vnum <= top_vnum_obj; vnum++)

        {

        if((obj=get_obj_index(vnum)) != NULL)

            {

            if((obj->item_type == type && type2 == 0

                && str_cmp(arg1,"weapon") && str_cmp(arg1,"armor"))

            || (obj->item_type == type && obj->value[0] == type2

                && str_cmp(arg1,"armor"))

            || (obj->item_type == type && IS_SET(obj->wear_flags,type2)

                && str_cmp(arg1,"weapon")))    

                {

                    sprintf(buf, "%35s - %5d - Area [%d]\n\r", obj

->short_descr, vnum, obj->area->vnum);   

                    found = TRUE;

                    strcat(buffer,buf);

                }

            }

        }

    if (!found)

        send_to_char("No objects of that type exist\n\r",ch);

    else            

        if (ch->lines)

            page_to_char(buffer,ch);

        else

            send_to_char(buffer,ch);

}   
