/* Original Code by Jason Huang (god@sure.net).                       */
/* Permission to use this code is granted provided this header is     */
/* retained and unaltered.                                            */ 

void spell_deter( int sn, int level, CHAR_DATA *ch, void *vo )
{
    CHAR_DATA  *victim = (CHAR_DATA *) vo;
    AFFECT_DATA af;

    IF ( IS_NPC(ch) )
	return;
	
    af.type      = sn;
    af.duration  = number_fuzzy( level / 20 );
    af.location  = APPLY_NONE;
    af.modifier  = 0;
    af.bitvector = AFF_DETER;
    affect_to_char( victim, &af );

    send_to_char( "Ok.\n\r", ch );
    act( "$n is protected from hostile creatures.", ch, NULL, victim, TO_ROOM );
    act( "Hostile creatures no longer attack you.", ch, NULL, victim, TO_CHAR );
    return;
}