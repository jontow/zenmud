The following is required if part I was installed succesfully...
Warning: BACKUP all the files and areas befoe proceeding, failing in 
doing so can cause a serious damage to your mud if not followed 
properly...
In order to edit the mobprogram code online, two things need to be 
done, collect all the code in _ONE_ file and avoid loading it as a an 
area, but as a mobprogram itself, to do so some changes must be done 
to your db.c, make sure you have mobprograms for Rom2.4 
installed properly...

Add in the following in:

[Makefile]
olc_mpcode.o

[merc.h]
extern          int                     top_mprog_index;

[mem.c]
MPROG_CODE              *       mpcode_free;

MPROG_CODE *new_mpcode(void)
{
     MPROG_CODE *NewCode;

     if (!mpcode_free)
     {
         NewCode = alloc_perm(sizeof(*NewCode) );
         top_mprog_index++;
     }
     else
     {
         NewCode     = mpcode_free;
         mpcode_free = mpcode_free->next;
     }

     NewCode->vnum    = 0;
     NewCode->code    = str_dup("");
     NewCode->next    = NULL;

     return NewCode;
}

void free_mpcode(MPROG_CODE *pMcode)
{
    free_string(pMcode->code);
    pMcode->next = mpcode_free;
    mpcode_free  = pMcode;
    return;
}

[db.c]
int                     top_mprog_index;

in void load_ mobprogs() comment these lines:
    /*if ( area_last == NULL )
    {
        bug( "Load_mobprogs: no #AREA seen yet.", 0 );
        exit( 1 );
    }*/

after 
        else
        {
           pMprog->next = mprog_list;
           mprog_list   = pMprog;
        }
add
        top_mprog_index++;

also add this with the other defines:

#define ED_MPCODE <number>

Add this as well:

void    mpedit          args( ( CHAR_DATA *ch, char *argument ) );

[olc.h]

extern const struct olc_cmd_type        mpedit_table[];
DECLARE_DO_FUN( do_mpedit       );
DECLARE_OLC_FUN( redit_mpshow           );
MPROG_CODE       *new_mpcode             args ( (void) );
void             free_mpcode             args ( ( MPROG_CODE *pMcode));

[interp.h]
DECLARE_DO_FUN( do_mpsave       );
DECLARE_DO_FUN( do_mpedit       );

[interp.c]
  { "mpsave",         do_mpsave,      POS_DEAD,    IM,  LOG_NORMAL, 1 },
  { "mpedit",         do_mpedit,      POS_DEAD,    IM,  LOG_NORMAL, 1 },

[olc.c]
in run_olc_editor add
    case ED_MPCODE:
        mpedit( d->character, d->incomm );
        break;
before
    default:
        return FALSE;
    }
    return TRUE;
   }

in olc_ed_name add:
    case ED_MPCODE:
        sprintf( buf, "MPEdit" );
        break;

    before
    default:
        sprintf( buf, " " );
        break;

in olc_ed_vnum add:
    MPROG_CODE *pMcode;
and add
    case ED_MPCODE:
        pMcode = (MPROG_CODE *)ch->desc->pEdit;
        sprintf(buf, "%d", pMcode ? pMcode->vnum : 0);
        break;
before
    default:
        sprintf( buf, " " );

in show_commands add:
          case ED_MPCODE:
          show_olc_cmds( ch, mpedit_table );
          break;
before
        }

    return FALSE;
  }

[merc.h]

define this somewhere:

/* This will give you a max of 99999 mobprogs to work with */

#define MAX_VNUM 99999 

[olc_save.c]
void save_mobprogs( FILE *fp, MPROG_CODE *pMprog )
{
        int i;

        fprintf(fp, "#MOBPROGS\n");

        for (i=0; i< MAX_VNUM; i++)
        {
          if ( (pMprog = get_mprog_index(i) ) !=NULL)
          fprintf(fp, "#%d\n%s~\n",
             i, fix_string(pMprog->code));
        }

        fprintf(fp,"#0\n\n");
        return;
}

void save_mobprog( MPROG_CODE *pMcode )
{
     FILE *fp;

     fclose(fpReserve);
     if (! (fp = fopen( "mobprog.prg", "w") ) )
     {
        bug( "Open_help: fopen", 0);
        perror( "mobprog.prg");
     }
     save_mobprogs(fp, pMcode);
     fprintf(fp, "#$\n" );

     fclose( fp );
     fpReserve = fopen( NULL_FILE, "r" );

     return;
}

void do_mpsave( CHAR_DATA *ch, char *argument )
{
   MPROG_CODE *pMcode;
   FILE *fp;

   if (!IS_IMMORTAL(ch))
   {
        send_to_char("Huh?\n\r",ch);
        return;
   }

   fp = NULL;
   if ( ch->desc->editor == 0)
   {
        send_to_char( "You are not editing a mobprogram code\n\r",ch);
        return;
   }

   pMcode = (MPROG_CODE *)ch->desc->pEdit;

   save_mobprog( pMcode );
   send_to_char( "Code saved.\n\r",ch);
   return;
}

in save_area_list() add:
        fprintf( fp, "mobprog.prg\n" );
after
        fprintf( fp, "help.are\n"   );


and finally make a new file and call it olc_mpcode.c:

/***************************************************************************
 *  File: olc_mpcode.c                                                     *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 *                                                                         *
 * The following code is based on ILAB OLC by Jason Dinkel                 *
 * Mobprogram code by Lordrom for Nevermore Mud                            *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *									   *
 *  ROM OLC for 2.4 was made to work by Beowolf and Nikki. We thank        *
 *  Airius and all the others from the ROM mailing list and the Merc       *
 *  mailing lists as well. Many hours have been put into this version      *
 *  We at The Shadow Realms MUD (tsr.org 8000) hope you enjoy this as      *
 *  much as we do.                                                         *
 *									   *
 ***************************************************************************/

#if defined(macintosh)
#include <types.h>
#else
#include <sys/types.h>
#endif
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "merc.h"
#include "olc.h"

#define MPEDIT( fun )           bool fun(CHAR_DATA *ch, char*argument) 
#define EDIT_MPCODE(Ch, Code)   ( Code = (MPROG_CODE*)Ch->desc->pEdit )

DECLARE_OLC_FUN( mpedit_create );
DECLARE_OLC_FUN( mpedit_desc   );
DECLARE_OLC_FUN( mpedit_show   );

const struct olc_cmd_type mpedit_table[] =
{
/*     { command           function    }, */

       { "commands",       show_commands },
       { "create",         mpedit_create },
       { "desc",           mpedit_desc   },
       { "show",           mpedit_show   },
       { "?",              show_help     },

       { "",                 0           }
};

void mpedit( CHAR_DATA *ch, char *argument)
{
    MPROG_CODE *pMcode;
    char arg[MAX_INPUT_LENGTH];
    char command[MAX_INPUT_LENGTH];
    int cmd;

    smash_tilde(argument);
    strcpy(arg, argument);
    argument = one_argument( argument, command);

    EDIT_MPCODE(ch, pMcode);
    if (ch->pcdata->security < 7)
    {
        send_to_char("MPEdit: Insufficient security to modify
        code\n\r",ch); edit_done(ch);
    }

    if (command[0] == '\0')
    {
        mpedit_show(ch, argument);
        return;
    }
    if (!str_cmp(command, "done") )
    {
        edit_done(ch);
        return;
    }

    for (cmd = 0; mpedit_table[cmd].name[0] != '\0'; cmd++)
    {
        if (!str_prefix(command, mpedit_table[cmd].name) )
        {
           (*mpedit_table[cmd].olc_fun) (ch, argument);
           return;
        }
    }

    interpret(ch, arg);
    return;
}

void do_mpedit(CHAR_DATA *ch, char *argument)
{
    MPROG_CODE *pMcode;
    char command[MAX_INPUT_LENGTH];

    argument = one_argument( argument, command);
    if(is_number(command) )
    {
       if (! (pMcode = get_mprog_index( atoi(command) ) ) )
       {
           send_to_char("MPEdit: That vnum does not exist.\n\r",ch);
           return;
       }
       ch->desc->pEdit=(void *)pMcode;
       ch->desc->editor= ED_MPCODE;
       return;
    }

    if (!str_cmp(command, "create" ) )
    {
        if (argument[0] == '\0')
        {
           send_to_char("Syntax: edit code create [vnum]\n\r",ch);
           return;
        }
        if (mpedit_create(ch, argument) )
        ch->desc->editor = ED_MPCODE;
    }

    return;
}

MPEDIT (mpedit_create)
{
    MPROG_CODE *pMcode;
    int value;

    value = atoi(argument);
    if (argument[0] == '\0' || value == 0)
    {
        send_to_char("Syntax: mpedit create [vnum]\n\r",ch);
        return FALSE;
    }

    if (get_mprog_index(value) )
    {
        send_to_char("MPEdit: Code vnum already exists.\n\r",ch);
        return FALSE;
    }

    pMcode                        = new_mpcode();
    pMcode->vnum                  = value;
    pMcode->next                  = mprog_list;
    mprog_list                    = pMcode;
    ch->desc->pEdit               = (void *)pMcode;

    send_to_char("MobProgram Code Created.\n\r",ch);
    return TRUE;
}

MPEDIT( mpedit_show)
{
    MPROG_CODE *pMcode;
    char buf[MAX_STRING_LENGTH];


    EDIT_MPCODE(ch,pMcode);

    sprintf(buf,
           "Vnum:       [%d]\n\r"
           "Code:\n\r%s\n\r",
           pMcode->vnum, pMcode->code);
    send_to_char(buf, ch);

    return FALSE;
}

MPEDIT( mpedit_desc)
{
    MPROG_CODE *pMcode;
    EDIT_MPCODE(ch, pMcode);

    if (argument[0] =='\0')
    {
       string_append(ch, &pMcode->code);
       return TRUE;
    }

    send_to_char(" Syntax: desc\n\r",ch);
    return FALSE;
}



------------------------ End of olc_mpcode.c ----------------------------------

compile everything, remove any mobprogram code in your areas not mobprogram assignments just the code
and place them all in mobprog.prg (default mobprogram file from now on)

for instance:
#1
say hello $n!
smile
~

make sure your file has something like this:

#MOBPROGS
#1
say hello $n!
smile
~
other mobprograms here (sorted :))
start up the mud and edit a mobprogram
mpedit create <vnum>
show
desc
etc...
then type mpsave to save it, please type mpsave when you are sure the 
code works, ie your mobprogram code, else your mud won't boot up...
if everything else goes well and you get going then you're on your 
way, oh one more thing you might need this nice tool in mob_cmds.c :)

first include recycle.h in mob_cmds.c so make sure you have this:
#include "recycle.h"

then in interp.c:
    { "mplist",         do_mplist,      POS_DEAD,       IM, LOG_NEVER,  1 },

and then in mob_cmds.c add:

void do_mplist( CHAR_DATA *ch, char *argument )
{
    int count;
    MPROG_CODE *mprg;
    char buf[MAX_STRING_LENGTH];
    BUFFER *buffer;
    buffer=new_buf();

    for (count =1, mprg= mprog_list; mprg !=NULL; mprg = mprg->next) 
    {
      sprintf(buf, "[%3d] %5d\n\r", count, mprg->vnum);
      add_buf(buffer, buf);
      count++;
    }
    page_to_char(buf_string(buffer), ch);
    free_buf(buffer);
    return;
}

I guess I covered everything, good luck everyone and happy mobprogramming :)

Lordrom 
