From: Lotus <headn@okstate.edu>
Subject: Non-Debugged Multi-Command

I posted a note recently about stacking commands within the mud.  I'd 
like to thank those who responded with some better ideas =).  I've 
included with this note a working(?) version of the command.  Please feel 
wrong/could have done better, let me know =).



Add this to the top of comm.c and the top of alias.c:

char    *get_multi_command      args((DESCRIPTOR_DATA *d, 
                                                char *argument))

Add this in comm.c somewhere:

char                command[MAX_INPUT_LENGTH];

char *get_multi_command( DESCRIPTOR_DATA *d, char *argument )
{
    int counter, counter2;
    char leftover[MAX_INPUT_LENGTH];

    command[0] = '\0';

    for ( counter = 0; argument[counter] != '\0'; counter++ )
    {
        if ( argument[counter] == '|' &&  argument[counter+1] != '|' )
        {
             command[counter] = '\0';
             counter++;
             for (counter2 = 0; argument[counter] != '\0';
                  counter2++,counter++)
                 leftover[counter2] = argument[counter];
             leftover[counter2] = '\0';
             strcpy( d->incomm,leftover );
             return (command);
        }
        else if (argument[counter] == '|' && argument[counter+1] == '|')
            for (counter2 = counter; argument[counter2] != '\0';
                            counter2++)
                argument[counter2] = argument[counter2+1];

        command[counter] = argument[counter];

    }
    d->incomm[0] = '\0';
    command[counter] = '\0';
    return (command);
}

Add this to game_loop_unix to replace what is there:

            if ( d->incomm[0] == '\0' )
                read_from_buffer( d );

            if ( d->incomm[0] != '\0' )
            {
                char *command2;

                d->fcommand     = TRUE;
                stop_idling( d->character );

                command2 = get_multi_command( d, d->incomm );
                if ( d->connected == CON_PLAYING )
                    substitute_alias( d, command2 );
                else
                    nanny( d, command2 );
            }

Add this to substitute_alias in alias.c

            if (!strcmp(ch->pcdata->alias[alias],name))
            {
                buf[0] = '\0';
                strcat(buf,ch->pcdata->alias_sub[alias]);
                strcat(buf," ");
                strcat(buf,point);
/* This line */ strcpy( buf,get_multi_command( d, buf));
                break;
            }

