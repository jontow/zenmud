By request the following code is needed to get OLC in mobprograms, 
the code is divided in 3 sections, the declarations, the editing, and 
(please note that this code will only edit mobprograms on mobs and not 
the mobprograms code, in the next email i will post the code to do so)

-Lordrom 

DISCLAIMER: A few simple things were changed from Lordrom's release to get
            it to work with my OLC. All I did was convert some. So If you
            have any questions on mobprog OLC ask Lordrom.

Declarations:
---------------
Before doing anything we need to declare the functions that are needed.

 in recycle.c add the following:

/* stuff for recycling mobprograms */
MPROG_LIST *mprog_free;

MPROG_LIST *new_mprog(void)
{
   static MPROG_LIST mp_zero;
   MPROG_LIST *mp;

   if (mprog_free == NULL)
       mp = alloc_perm(sizeof(*mp));
   else
   {
       mp = mprog_free;
       mprog_free=mprog_free->next;
   }

   *mp = mp_zero;
   mp->vnum             = 0;
   mp->trig_type        = 0;
   mp->code             = str_dup("");
   VALIDATE(mp);
   return mp;
}

void free_mprog(MPROG_LIST *mp)
{
   if (!IS_VALID(mp))
      return;

   INVALIDATE(mp);
   mp->next = mprog_free;
   mprog_free = mp;
}

in recycle.h declare the following:

/* mobprog recycling */
#define MPL MPROG_LIST
MPL     *new_mprog args( (void) );
void    free_mprog args( (MPROG_LIST *mp) );
#undef MPL

in mem.c add:
    pMob->mprogs        =   NULL;
to new_mob_index()

add:
    free_mprog( pMob->mprogs );
to free_mob_index()

in merc.h add:
    bool                valid;
to mprog_list structure

in olc.h add:
DECLARE_OLC_FUN( medit_addmprog         ); /* Lordrom */
DECLARE_OLC_FUN( medit_delmprog         ); /* Lordrom */

and add:
MPROG_LIST      *new_mprog              args ( ( void ) );
void            free_mprog              args ( ( MPROG_LIST *mp ) );

in olc.c add:
    {   "addmprog",     medit_addmprog  },
    {   "delmprog",     medit_delmprog  },
to medit_table

Editing
--------
here is the code that you need to add mobprograms to mobs you edit in 
OLC, so far all the code is just for assigning mobprograms not for 
editing the mobprogram code, in the next email i will post the OLC 
code for mprog_code which should not be confused with mprog_list

in olc_act.c function medit_show add:
    MPROG_LIST *list; 
at the beginning of the function and add:

  if ( pMob->mprogs )
  {
    
    MPROG_LIST *list;
    int cnt = 0;
   
	sprintf(buf,
		"\n\rMOBPrograms for [%5d]:\n\r", pMob->vnum);
	send_to_char(buf, ch);

        for (cnt = 0, list=pMob->mprogs; list; list=list->next)
        {
             if (cnt ==0)
               {
                 send_to_char("Number Vnum Trigger Phrase\n\r", ch);
                 send_to_char("------ ---- ------- ------\n\r", ch);
               }
          
	         sprintf(buf, "[%5d] %4d %7s %s\n\r", 
		 cnt,
                 list->vnum,
		 mprog_type_name(list->trig_type),
                 list->trig_phrase);
		 cnt++;
                 send_to_char(buf, ch);
        }
   }
    return FALSE;
}

after the shop check.

Here is the code for both functions:

MEDIT ( medit_addmprog )
{
  int value;
  MOB_INDEX_DATA *pMob;
  MPROG_LIST *list;
  MPROG_CODE *code;
  char trigger[MAX_STRING_LENGTH];
  char phrase[MAX_STRING_LENGTH];
  char num[MAX_STRING_LENGTH];

  EDIT_MOB(ch, pMob);
  argument=one_argument(argument, num);
  argument=one_argument(argument, trigger);
  argument=one_argument(argument, phrase);

  if (!is_number(num) || trigger[0] =='\0' || phrase[0] =='\0' )
  {
        send_to_char("Syntax: addmprog [vnum] [trigger] [phrase]\n\r", ch);
	return FALSE;
  }

  if ( (value = flag_value (mprog_flags, trigger) ) == NO_FLAG)
  {
        send_to_char("Valid flags are:\n\r",ch);
        show_help( ch, "? mpflags");
        return FALSE;
  }

  if ( ( code =get_mprog_index (atoi(num) ) ) == NULL)
  {
        send_to_char("No such MOBPrograms\n\r",ch);
        return FALSE;
  }

  list                  = new_mprog();
  list->vnum            = atoi(num);
  list->trig_type       = value;
  list->trig_phrase     = str_dup(phrase);
  list->code            = code->code;
  pMob->mprog_flags     = value;
  list->next            = pMob->mprogs;
  pMob->mprogs          = list;

  send_to_char( "Mprog Added.\n\r",ch);
  return TRUE;
}

MEDIT ( medit_delmprog )
{
    MOB_INDEX_DATA *pMob;
    MPROG_LIST *list;
    MPROG_LIST *list_next;
    char mprog[MAX_STRING_LENGTH];
    int value;
    int cnt = 0;

    EDIT_MOB(ch, pMob);

    one_argument( argument, mprog );
    if (!is_number( mprog ) || mprog[0] == '\0' )
    {
       send_to_char("Syntax:  delmprog [#mprog]\n\r",ch);
       return FALSE;
    }

    value = atoi ( mprog );

    if ( value < 0 )
    {
        send_to_char("Only non-negative mprog-numbers allowed!!!\n\r",ch);
        return FALSE;
    }

    if ( !(list= pMob->mprogs) )
    {
        send_to_char("MEdit:  Non existant mprog.\n\r",ch);
        return FALSE;
    }

    if ( value == 0)
    {
        list = pMob->mprogs;
        pMob->mprogs = list->next;
        free_mprog( list );
    }
    else
    {
        while ( (list_next = list->next) && (++cnt < value ) )
                list = list_next;

        if ( list_next )
        {
                list->next = list_next->next;
                free_mprog( list_next);
        }
        else
        {
                send_to_char("No such mprog.\n\r",ch);
                return FALSE;
        }
    }

    send_to_char("Mprog removed.\n\r", ch);
    return TRUE;
}

Saving
--------
This portion is needed for your changes to be saved, remmeber that 
all the code is only for adding mobprograms provided that the 
mobprograms code already exist, in the next email the code for 
editing the mobprogram code will be added, i did this so you can 
apply the first part first and test it for bugs.

In olc_save.c save_mobile() add:

 	MPROG_LIST *pMprog;

at the beginning and add:
    
    for (pMprog=pMobIndex->mprogs;pMprog;pMprog=pMprog->next)
    {
        fprintf( fp, "M %s",           mprog_type_name(pMprog->trig_type));
        fprintf( fp, "%d ",            pMprog->vnum                      );
        fprintf( fp, "%s~\n",          pMprog->trig_phrase               );
    }

right after:
    fprintf( fp, "%d\n",          pMobIndex->material );

in merc.h add this:

extern 	const	struct  flag_type	mprog_flags[];  /* MOBPROGS */

in tables.c add this (or bit.c whichever you prefer.):

/* MOBPROGS */
const struct flag_type mprog_flags[] =
{
    {	"act",			TRIG_ACT,		TRUE	},
    {	"bribe",		TRIG_BRIBE,		TRUE 	},
    {	"death",		TRIG_DEATH,		TRUE    },
    {	"entry",		TRIG_ENTRY,		TRUE	},
    {	"fight",		TRIG_FIGHT,		TRUE	},
    {	"give",			TRIG_GIVE,		TRUE	},
    {	"greet",		TRIG_GREET,		TRUE    },
    {	"grall",		TRIG_GRALL,		TRUE	},
    {	"kill",			TRIG_KILL,		TRUE	},
    {	"hpcnt",		TRIG_HPCNT,		TRUE    },
    {	"random",		TRIG_RANDOM,		TRUE	},
    {	"speech",		TRIG_SPEECH,		TRUE	},
    {	"exit",			TRIG_EXIT,		TRUE    },
    {	"exall",		TRIG_EXALL,		TRUE    },
    {	"delay",		TRIG_DELAY,		TRUE    },
    {	"surr",			TRIG_SURR,		TRUE    },
    {   NULL,                   0,                      0       }
};

in the olc_act.c help_table add this:

    {"mpflag", mprog_flags, "Mobprog trigger flags."},

Almost forgot this:

handler.c: 
---------- 
add this somewhere in the file:

char *mprog_type_name( int mprog_flags )
{
    
    static char buf[512];

    buf[0] = '\0';
   
    if (mprog_flags & TRIG_ACT    ) strcat(buf, " act ");
    if (mprog_flags & TRIG_SPEECH ) strcat(buf, " speech ");
    if (mprog_flags & TRIG_RANDOM ) strcat(buf, " random ");
    if (mprog_flags & TRIG_FIGHT  ) strcat(buf, " fight ");
    if (mprog_flags & TRIG_HPCNT  ) strcat(buf, " hpcnt ");
    if (mprog_flags & TRIG_DEATH  ) strcat(buf, " death ");
    if (mprog_flags & TRIG_ENTRY  ) strcat(buf, " entry ");
    if (mprog_flags & TRIG_GREET  ) strcat(buf, " greet ");
    if (mprog_flags & TRIG_GRALL  ) strcat(buf, " grall ");
    if (mprog_flags & TRIG_GIVE   ) strcat(buf, " give ");
    if (mprog_flags & TRIG_BRIBE  ) strcat(buf, " bribe ");
    if (mprog_flags & TRIG_KILL   ) strcat(buf, " kill ");
    if (mprog_flags & TRIG_DELAY  ) strcat(buf, " delay ");
    if (mprog_flags & TRIG_SURR   ) strcat(buf, " surr ");
    if (mprog_flags & TRIG_EXIT   ) strcat(buf, " exit ");
    if (mprog_flags & TRIG_EXALL  ) strcat(buf, " exall ");
    
    return (buf[0] != '\0' ) ? buf+1 : "none";

}


if everything goes well you will be able to add mobprograms to any 
mobs you have provided that the mobprogram code already exists, I 
repeat again that this won't edit the mobprogram code but only adds a 
mobprogram to mobs for example:

You want to add mobprogram 100 to mob 200 with trigger greet and 
phrase 100 you do the following:

edit mob 200
addmprog 100 greet 100
( if the mobprogram code 100 exists it will say)
Mprog added
(else it will say No such Mobprogram)

Good luck, and please email me if you have problems or to the list, 
Thanks

One more thing, if you use the code please credit me :)

Stay tuned for OLC for editing mobprograms code...

Lordrom 
