From: dennis@starlifter.reichel.net

> > Humm.. what you will probally need to do is to parse the keywords
> > yourself, and i don't think that you can have spaces in them.. i think a
> > space seperates keywords, what you need to do is a have some code that
> > looks like this.
> 
> > 	word = strtok(phelp->keywords,seps)
> 
> Almost, except use one_argument rather than strtok; help keywords can have
> space in them, e.g. 'magic missile'. 
> 

I achieved a similar effect more simply :-)  

help <one character>

Will display the keywords only that begin with that letter.  It works fine
on my server - anyone is welcome to drop it into their rom, I require no
credit or postcards, etc :-)


void do_help( CHAR_DATA *ch, char *argument )
{
    HELP_DATA *pHelp;
    BUFFER *output;
    bool found = FALSE;
    char argall[MAX_INPUT_LENGTH],argone[MAX_INPUT_LENGTH];
    int level;

    output = new_buf();

    if ( argument[0] == '\0' )
	argument = "summary";

    /* this parts handles help a b so that it returns help 'a b' */
    argall[0] = '\0';
    while (argument[0] != '\0' )
    {
	argument = one_argument(argument,argone);
	if (argall[0] != '\0')
	    strcat(argall," ");
	strcat(argall,argone);
    }

    for ( pHelp = help_first; pHelp != NULL; pHelp = pHelp->next )
    {
    	level = (pHelp->level < 0) ? -1 * pHelp->level - 1 : pHelp->level;

	if (level > get_trust( ch ) )
	    continue;

	if ( is_name( argall, pHelp->keyword ) )
	{
	    /* add seperator if found */
	    if (found)
		add_buf(output,
    "\n\r#W============================================================#n\n\r\n\r");
//    Someone edited my helps and put all the 0's to -1 so.... :-)
//	    if ( pHelp->level >= 0 && str_cmp( argall, "imotd" ) ) 
	    if ( pHelp->level >= -1 && str_cmp( argall, "imotd" ) )
	    {
		add_buf(output,pHelp->keyword);
		add_buf(output,"\n\r");
	    }

	    /*
	     * Strip leading '.' to allow initial blanks.
	     */
	    if ( strlen( argall ) == 1 )   
	        ;     // DJR HACK - display only keywords 	       	    
	    else if ( pHelp->text[0] == '.' )
		add_buf(output,pHelp->text+1);
	    else
		add_buf(output,pHelp->text);
	    found = TRUE;
	    /* small hack :) */
	    if (ch->desc != NULL && ch->desc->connected != CON_PLAYING 
	    &&  		    ch->desc->connected != CON_GEN_GROUPS)
		break;
	}
    }

    if (!found)
    	send_to_char( "No help on that word.\n\r", ch );
    else
	page_to_char(buf_string(output),ch);
    free_buf(output);
}
