From: Whiplash <whiplash@tft.nacs.net>
Subject: Hidden items/Search skill

Here's a bit of code I threw together that implements hidden items in your
MUD, this is different than invis items, in that a player can HIDE an item,
and then another player can SEARCH for it, to reveal it. We use SEARCH as a
skill, and our skill_table stuff is hella different from Merc/ROM, so I
haven't included it here, but it's better that you decide what classes, and
levels you want this skill to affect anyway, other than that, everything
should be included here. If you use OLC, you'll wana add support for the
ITEM_HIDDEN bit in bit.c

Oh yeah, I don't require any credit for this..
but you can't take any either :)


/* Hidden Items by whiplash@tft.nacs.net from South of Heaven MUD */

/* in merc.h add  */
extern  sh_int     gsn_search;

/* add the new extra_flag bit for objects */
#define ITEM_HIDDEN          (xx) /* whatever you have free */

/* in db.c, in the rest of the gsn's add */

sh_int  gsn_search;

/* in handler.c, in bool can_see_obj add */

if(IS_SET(obj->extra_flags,ITEM_HIDDEN))
return FALSE;

/* in act_info.c in char format_obj_to_char */
if(IS_OBJ_STAT(obj,ITEM_HIDDEN)) strcat(buf,"(Hidden) ");

/*
 * This command checks for hidden objects
 */
void do_search( CHAR_DATA *ch, char *argument )
 {
   OBJ_DATA *obj;

   if(IS_NPC(ch)) return;

   if(ch->pcdata->learned[gsn_search] < 1) {
   send_to_char("You search around clumsily.\n\r",ch);
   return;
   }

   if(number_percent() < ch->pcdata->learned[gsn_search] )
   {
    check_improve(ch,gsn_search,TRUE,4);
    send_to_char("You search the room..\n\r",ch); 

    for(obj = ch->in_room->contents; obj != NULL; obj = obj->next_content)
    {
    if(IS_SET(obj->extra_flags,ITEM_HIDDEN) )
    {
    printf_to_char(ch,"You reveal %s\n\r",obj->short_descr);
     REMOVE_BIT(obj->extra_flags,ITEM_HIDDEN);     
       }
      }
       send_to_char("You have searched everywhere.\n\r",ch);
       WAIT_STATE(ch,24);
      }
      else
      send_to_char("You didn't uncover anything unusual.\n\r",ch);
     check_improve(ch,gsn_search,FALSE,4);
    WAIT_STATE(ch,24);
   return;
 }

/*
 * Modified "Hide" skill, this allows hiding objects as well.
 */
void do_hide( CHAR_DATA *ch, char *argument )
{

/*===== new from here.. */
  OBJ_DATA *obj; 
  char arg[MAX_INPUT_LENGTH];  

   argument = one_argument(argument, arg );

   if(arg[0] != '\0')
    {
     if(( obj = get_obj_carry(ch,arg)) == NULL)
     {
     send_to_char("You don't have that item.\n\r",ch);
     return;
     }
    obj_from_char(obj);
    SET_BIT(obj->extra_flags,ITEM_HIDDEN);
    obj_to_room(obj,ch->in_room);
    act("$n hides $p.",ch,obj,NULL,TO_ROOM);
    act("You hide $p.",ch,obj,NULL,TO_CHAR);
    return;
   }
 else

/*===== to here.. */

    send_to_char( "You attempt to hide.\n\r", ch );

    if ( IS_AFFECTED(ch, AFF_HIDE) )
	REMOVE_BIT(ch->affected_by, AFF_HIDE);

    if ( IS_NPC(ch) || number_percent( ) < ch->pcdata->learned[gsn_hide] )
    {
	SET_BIT(ch->affected_by, AFF_HIDE);
	check_improve(ch,gsn_hide,TRUE,3);
    }
    else
	check_improve(ch,gsn_hide,FALSE,3);

    return;
}

