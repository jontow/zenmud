From: ferris@FootPrints.net

A friend of mine told me about this really cool stdio function called
popen. I love it! I went ahead and wrote some code for it.

Here's what it does and how to add it:
1) You add this file to your Makefile
2) Add lines in interp.c and interp.h, make it IMP only (the reason is
that this gives direct access to your code)
3) Now you can type "pipe ps" and list processes or type "pipe ls" and
list files... etc, etc.

WARNING:
Some imps might think this is too much of a breach of security.
Use at your own risk!

NOTE:
You could alter the main function and have smaller (weaker) functions make
a call to it so as too keep all security.

-------------------[CUT HERE pipe.c]----------------------------

#if defined(macintosh)
#include <types.h>
#include <time.h>
#else
#include <sys/types.h>
#include <sys/time.h>
#endif
#include <stdio.h>
#include "merc.h"

/*
 * Local functions.
 */
FILE 		*	popen		args( ( const char *command, const char *type ) );
int 			pclose		args( ( FILE *stream ) );
char		*	fgetf		args( ( char *s, int n, register FILE *iop ) );

void do_pipe( CHAR_DATA *ch, char *argument )
{
    char buf[5000];
    FILE *fp;

    fp = popen( argument, "r" );

    fgetf( buf, 5000, fp );

    page_to_char( buf, ch );

    pclose( fp );

    return;
}

char *fgetf( char *s, int n, register FILE *iop )
{
    register int c;
    register char *cs;

    c = '\0';
    cs = s;
    while( --n > 0 && (c = getc(iop)) != EOF)
	if ((*cs++ = c) == '\0')
	    break;
    *cs = '\0';
    return((c == EOF && cs == s) ? NULL : s);
}
