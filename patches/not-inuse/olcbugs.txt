From: dennis@starlifter.reichel.net

> I have recently found a bug in the OLC, I am sure that others have found
> this as it is one of the first things to come across.  The bug is in
> REDIT when you go to edit the name of a door and try to set the keyword
> of the door as "DOOR" it just toggles the exit flag.  Please let me know
> if someone has a fix for this, thanks..  Also wondered if there was an

Hello Jeff,

This corrects that - it's not beautiful, but it's adequate.  
Other changes:

Exit flags on both sides are synched
Value of -1 for key is allowed
If flag for "door" is not set, no other flags will be set

Note: these changes were made in a hurry - and have lots of
room for improvement.

Dennis

/* Local function. */
bool change_exit( CHAR_DATA *ch, char *argument, int door )
{
    ROOM_INDEX_DATA *pRoom;
    char command[MAX_INPUT_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    int  value;

    EDIT_ROOM(ch, pRoom);

    /*
     * Set the exit flags, needs full argument.
     * ----------------------------------------
     */
    if ( ( ( value = flag_value( exit_flags, argument ) ) != NO_FLAG ) 
       && ( str_cmp( argument, "name door" ) ) )
    {
	ROOM_INDEX_DATA *pToRoom;
	sh_int rev;                                    /* ROM OLC */

	if ( !pRoom->exit[door] )
	   {
	   	send_to_char("Exit does not exist.\n\r",ch);
	   	return FALSE;
	   }
	 /*   pRoom->exit[door] = new_exit(); */

	/*
	 * This room.
	 */
	TOGGLE_BIT(pRoom->exit[door]->rs_flags,  value);

	if (!(EX_ISDOOR & pRoom->exit[door]->rs_flags))
	    pRoom->exit[door]->rs_flags = 0;
	
	pRoom->exit[door]->exit_info = pRoom->exit[door]->rs_flags;

	/*
	 * Connected room.
	 */
	pToRoom = pRoom->exit[door]->u1.to_room;     /* ROM OLC */
	rev = rev_dir[door];

	if (pToRoom->exit[rev] != NULL)
	{
	    pToRoom->exit[rev]->rs_flags = pRoom->exit[door]->rs_flags;
	    pToRoom->exit[rev]->exit_info = pRoom->exit[door]->exit_info;	
	/* think this is afu - if both sides are not synched - we have mess  
	 * TOGGLE_BIT(pToRoom->exit[rev]->rs_flags,  value);
	 * TOGGLE_BIT(pToRoom->exit[rev]->exit_info, value);
	 */
	}

	send_to_char( "Exit flag toggled.\n\r", ch );
	return TRUE;
    }

    /*
     * Now parse the arguments.
     */
    argument = one_argument( argument, command );
    one_argument( argument, arg );

    if ( command[0] == '\0' && argument[0] == '\0' )	/* Move command. */
    {
	move_char( ch, door, TRUE );                    /* ROM OLC */
	return FALSE;
    }

    if ( command[0] == '?' )
    {
	do_help( ch, "EXIT" );
	return FALSE;
    }

    if ( !str_cmp( command, "delete" ) )
    {
	ROOM_INDEX_DATA *pToRoom;
	sh_int rev;                                     /* ROM OLC */
	
	if ( !pRoom->exit[door] )
	{
	    send_to_char( "REdit:  Cannot delete a null exit.\n\r", ch );
	    return FALSE;
	}

	/*
	 * Remove ToRoom Exit.
	 */
	rev = rev_dir[door];
	pToRoom = pRoom->exit[door]->u1.to_room;       /* ROM OLC */
	
	if ( pToRoom->exit[rev] )
	{
	    free_exit( pToRoom->exit[rev] );
	    pToRoom->exit[rev] = NULL;
	}

	/*
	 * Remove this exit.
	 */
	free_exit( pRoom->exit[door] );
	pRoom->exit[door] = NULL;

	send_to_char( "Exit unlinked.\n\r", ch );
	return TRUE;
    }

    if ( !str_cmp( command, "link" ) )
    {
	EXIT_DATA *pExit;

	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax:  [direction] link [vnum]\n\r", ch );
	    return FALSE;
	}

	value = atoi( arg );

	if ( !get_room_index( value ) )
	{
	    send_to_char( "REdit:  Cannot link to non-existant room.\n\r", ch );
	    return FALSE;
	}

	if ( !IS_BUILDER( ch, get_room_index( value )->area ) )
	{
	    send_to_char( "REdit:  Cannot link to that area.\n\r", ch );
	    return FALSE;
	}

	if ( get_room_index( value )->exit[rev_dir[door]] )
	{
	    send_to_char( "REdit:  Remote side's exit already exists.\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	{
	    pRoom->exit[door] = new_exit();
	}

	pRoom->exit[door]->u1.to_room = get_room_index( value );   /* ROM OLC */
	pRoom->exit[door]->orig_door = door;
	
/*	pRoom->exit[door]->vnum = value;                Can't set vnum in ROM */

	pRoom                   = get_room_index( value );
	door                    = rev_dir[door];
	pExit                   = new_exit();
	pExit->u1.to_room       = ch->in_room;
/*	pExit->vnum             = ch->in_room->vnum;    Can't set vnum in ROM */
	pExit->orig_door	= door;
	pRoom->exit[door]       = pExit;

	send_to_char( "Two-way link established.\n\r", ch );
	return TRUE;
    }
        
    if ( !str_cmp( command, "dig" ) )
    {
	char buf[MAX_STRING_LENGTH];
	
	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax: [direction] dig <vnum>\n\r", ch );
	    return FALSE;
	}
	
	redit_create( ch, arg );
	sprintf( buf, "link %s", arg );
	change_exit( ch, buf, door);
	return TRUE;
    }

    if ( !str_cmp( command, "room" ) )
    {
	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax:  [direction] room [vnum]\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	{
	    pRoom->exit[door] = new_exit();
	}

	value = atoi( arg );

	if ( !get_room_index( value ) )
	{
	    send_to_char( "REdit:  Cannot link to non-existant room.\n\r", ch );
	    return FALSE;
	}

	pRoom->exit[door]->u1.to_room = get_room_index( value );    /* ROM OLC */
	pRoom->exit[door]->orig_door = door;
/*	pRoom->exit[door]->vnum = value;                 Can't set vnum in ROM */

	send_to_char( "One-way link established.\n\r", ch );
	return TRUE;
    }

    if ( !str_cmp( command, "key" ) )
    {

	ROOM_INDEX_DATA *pToRoom;
	sh_int rev;                                    /* ROM OLC */

	if ( arg[0] == '\0' || !is_number( arg ) )
	{
	    send_to_char( "Syntax:  [direction] key [vnum]\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	   {
	   	send_to_char("Exit does not exist.\n\r",ch);
	   	return FALSE;
	   }

	value = atoi( arg );

        if (value != -1 )
        {
	    if ( !get_obj_index( value ) )
	    {
	        send_to_char( "REdit:  Item doesn't exist.\n\r", ch );
	        return FALSE;
	    }

	    if ( get_obj_index( atoi( argument ) )->item_type != ITEM_KEY)
	    {
		send_to_char( "REdit:  Key doesn't exist.\n\r", ch );
		return FALSE;
	    }
	}
	pRoom->exit[door]->key = value;
	
	pToRoom = pRoom->exit[door]->u1.to_room;     /* ROM OLC */
	rev = rev_dir[door];
	
	if ( pToRoom->exit[rev] )	
	    pToRoom->exit[rev]->key = pRoom->exit[door]->key; 

	send_to_char( "Exit(s) key set.\n\r", ch );
	
	return TRUE;
    }

    if ( !str_cmp( command, "name" ) )
    {
	if ( arg[0] == '\0' )
	{
	    send_to_char( "Syntax:  [direction] name [string]\n\r", ch );
	    send_to_char( "         [direction] name none\n\r", ch );
	    return FALSE;
	}

	if ( !pRoom->exit[door] )
	   {
	   	send_to_char("Salida no existe.\n\r",ch);
	   	return FALSE;
	   }

/*	if ( !pRoom->exit[door] )
	{
	    pRoom->exit[door] = new_exit();
	} */

	free_string( pRoom->exit[door]->keyword );
	if (str_cmp(arg,"none"))
		pRoom->exit[door]->keyword = str_dup( arg );
	else
		pRoom->exit[door]->keyword = str_dup( "" );

	send_to_char( "Exit name set.\n\r", ch );
	return TRUE;
    }

    if ( !str_prefix( command, "description" ) )
    {
	if ( arg[0] == '\0' )
	{
	   if ( !pRoom->exit[door] )
	   {
	   	send_to_char("Salida no existe.\n\r",ch);
	   	return FALSE;
	   }

/*	    if ( !pRoom->exit[door] )
	    {
	        pRoom->exit[door] = new_exit();
	    } */

	    string_append( ch, &pRoom->exit[door]->description );
	    return TRUE;
	}

	send_to_char( "Syntax:  [direction] desc\n\r", ch );
	return FALSE;
    }

    return FALSE;
}

-------
From: Doug Brewer <ingorian@FootPrints.net>

> work.  But ILabs OLC 1.6 is a good OLC package.  I, along with others I
> know, prefer to build offline.  So much more you can do faster.  But then
> use OLC to edit the areas, fixing stuff and changing stuff around, doing 
> desc's and such.

I agree with Lanthum.  I would recommend that you nab the string.c that
tow@shawn-faucher.umeres.maine.edu posted a few days ago, with a few
modifications it drops right in and is very nice.  (Thanks Tow)  There are
a few other things that I changed in OLC but nothing really a problem.

Things like changing the message unsecured people get from spanish to
'Huh?' when they type a command.  Changing it so that you can set and save
equip resets. (in the stock OLC when you convert your areas and do asave
world you overwrite all equipment resets to 0 so your 'unique' or seldom
loaded equipment repops everytime the mob does)  Fixing summoned or
transferred mobs so that they extract from the world allowing them to
repop in their area and not just hang around till killed or the mud
reboots. (Thanks Dennis)  And a few other tweaks that are specific to my
copy of code.  If you are interested I have a text file listing everything
I did and where in the code I did it to. :)

I would also recommend getting a plugin for OLC to allow you to edit your
helpfiles.  I nabbed the one from Erwin (Thanks Erwin) and after modifying
it per the instructions posted a few months back it worked like a charm. 
Actually, it worked fine in stock form, just that I had some special text
characters in my help files that needed the above mentioned mod to let it
save them correctly.  There is another online help editor but I forget who
wrote it, and since I have never used it, I don't have an opinion, it
should be on Darkoths site or maybe Kermits? And while you are patching,
:) nab Erwin's sedit code and mod/delete/create socials while online.

The seemingly dying breed of off-line area creators will probably still
prefer to build offline but OLC does make it simple to tweak and correct
areas that are online.  All in all I would say that I'm glad I added OLC,
even tho it took quite a bit of time to get it all working correctly in my
butchered up code base. :)

The next thing I plan to do to OLC is mod it so that when you add or
change an area that those areas are saved into a different directory.  On
reboot, if there is a problem with the areas I want it to load the old
area if it was a change or just skip over it if it's a new area.  But
that's down the road since I have the builders working on one port and the
working code on another.

Doug
-------
From: Doug Brewer <ingorian@FootPrints.net>
Subject: Re: MOB AC

On Tue, 10 Feb 1998, Darkoth wrote:
> I've been toying with a stock rom 2.4b4 with Ivan's OLC 1.6. Is it
> just me or is it whenever you edit a mobs ac that after you save
> and reboot it always goes back to 0?? Is this a known bug with
> his OLC? Just curious. 

Hi ya,

Was going through my mail and came across your message.  The same thing
happened to me until I removed the / 10 from the following code in
olc_save.c

    fprintf( fp, "%d %d %d %d\n", pMobIndex->ac[AC_PIERCE] / 10,
                                  pMobIndex->ac[AC_BASH]   / 10,
                                  pMobIndex->ac[AC_SLASH]  / 10,
                                  pMobIndex->ac[AC_EXOTIC] / 10 );

It's in save_mobile.  Not for sure why, but that seems to fix it. :)

Doug
-------
From: Paradise <cmosbob@erols.com>

not sure if this is a problem, or if its meant that way, but if someone
logs on to a ROM mud using the name
none, they will have full olc abilities...

the way i fixed it on my mud, was to make none an unacceptable name..

i found that in the parse name function, in comm.c, in with the names
honor, circle, etc...

just added none, and guest to that list, and now no one can log on as
that name...
-------
