From: Rich 'Wolflord' Bradshaw <nnzrik@unix.ccc.nottingham.ac.uk>

> Now that I'm babbling, has anyone put any time into that "room 
> description based on entry/exit direction" thing we were talking about 
> awhile back? It seems like the most detailed way to do it would be to 
> have a possible maximum of TWELVE room descriptions, one for each entry 
> direction, and one for each exit direction, this sounds like suddenly 
> using quite a bit more memory, or am I mistaken?
> 
> The cheesy alternative I've come up with in the meantime is a "day 
> description/name" "night description/name" for rooms based on time of day.

Yeah, I've put something along those lines in... It lets you give a
message to a player depending on which exit they use from a room.

In merc.h

struct  exit_data
{
    union
    {
        ROOM_INDEX_DATA *       to_room;
        sh_int                  vnum;
    } u1;
    sh_int              exit_info;
    sh_int              key;
    char *              keyword;
    char *              description;
    char *              message;	<-- added this
    EXIT_DATA *         next;
    int                 rs_flags;
    int                 orig_door;
};

----------
In act_move.c : void move_char( CHAR_DATA *ch, int door, bool follow )

/*
 * To have messages displayed to the character when moving between rooms.
 *  -- Diarmuid
 */
    if ( pexit->message && pexit->message[0] != '\0' )
    {
        send_to_char("\n\r\n\r", ch );
        send_to_char( pexit->message, ch );
        send_to_char("\n\r", ch );
    }

just before the line:
    do_look( ch, "auto" );

I put all the extra lines in as it kinda got missed otherwise. I've put it
in colour to help it stand out..

---------
In db.c : void load_room( FILE *fp )

when its reading in the doors, add:
                pexit->message          = fread_string( fp );

---------

I use OLC, so converting all the areas was pretty simple.

Added the code to the olc_save.c function for saving empty messages.
Compiled, asave world, then shutdown.

Add the code to the db.c, recompile, startup. Everything peachy ;-)

Wouldn't envy anyone implementing it this way if they were going to do it
by hand ;-)

I'm sure there's a better and less wasteful way, I implemented this a
while ago and kinda forgot about it for a bit.

Cheerio,
	Rich
