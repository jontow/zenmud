/* this code made by Sean Cohmer - Autodamage (you can also add this code
by just adding the things to fight.c and forgetting the rest of the auto stuff) */

@@@ act_info.c
   send_to_char("autoassist     ",ch);
   if (IS_SET(ch->act,PLR_AUTOASSIST))
       send_to_char("ON\n\r",ch);
   else
      send_to_char("OFF\n\r",ch); 

+   send_to_char("autodamage     ",ch);
+    if (IS_SET(ch->act,PLR_AUTODAMAGE))
+        send_to_char("ON\n\r",ch);
+    else
+        send_to_char("OFF\n\r",ch);


void do_autoassist(CHAR_DATA *ch, char *argument)
 {
    if (IS_NPC(ch))
      return;
        if (IS_SET(ch->act,PLR_AUTOASSIST))    {
      send_to_char("Autoassist removed.\n\r",ch);      
      REMOVE_BIT(ch->act,PLR_AUTOASSIST);    
   }    
    else    {      
      send_to_char("You will now assist when needed.\n\r",ch);
      SET_BIT(ch->act,PLR_AUTOASSIST); 
  }
}



+ void do_autodamage(CHAR_DATA *ch, char *argument)
+   {
+     if (IS_NPC(ch))
+      return;
+        if (IS_SET(ch->act,PLR_AUTODAMAGE))    {
+      send_to_char("Autodamage is currently OFF.\n\r",ch);      
+      REMOVE_BIT(ch->act,PLR_AUTODAMAGE);    
+   }    
+        else    {            
+      send_to_char("Autodamage is currently ON.\n\r",ch);
+      SET_BIT(ch->act,PLR_AUTODAMAGE); 
+   }
+ }

@@@ fight.c

        else
          {
	sprintf( buf1, "$n's %s %s $N%c",  attack, vp, dam, punct);
    	sprintf( buf2, "Your %s %s $N%c",  attack, vp, dam, punct);
    	sprintf( buf3, "$n's %s %s you%c", attack, vp, dam, punct);
          }

+      else
+      {
+      if {IS_SET(ch->act,PLR_AUTODAMAGE))
+      {
+        sprintf( buf1, "$n's %s %s $N%c",  attack, vp, punct );
+        sprintf( buf2, "Your %s %s $N%c [%d]",  attack, vp, punct, dam );
+        sprintf( buf3, "$n's %s %s you%c [%d]", attack, vp, punct, dam );
+      }

@@@ interp.c

{ "autoassist",	do_autoassist,	POS_DEAD,        0,  LOG_NORMAL, 1 },

+  { "autodamage", do_autodamage,     POS_DEAD,        0,  LOG_NORMAL, 1 },

@@@ interp.h

DECLARE_DO_FUN( do_autoassist	);

+  DECLARE_DO_FUN( do_autodamage    );

@@@ merc.h

#define PLR_AUTOASSIST		(C)

+   #define PLR_AUTODAMAGE            (D)

/*  note: change the rest of the alphabetical variables ie. D on autoexit
 *  would change to another variable (E) and so on for the rest of the RT
 *  auto flags..
 */

@@@ tables.c

{ "autoassist", 	C,	FALSE	},

+   { "autodamage",          D,    FALSE },

/*  note: you'll again have to change the alpha-variables so that the
 *  variables match the one's you just defined in merc.h...
 *
 *  thats also about all i need to put in, if you want to add in color
 *  to the autodamage code, add it yourself cause if you're doing this
 *  you should already know how to put color into this code... bye  8)
 */


