November 17th, 1997
Battle Prompts version 1.0

Here are my battle prompts, there are 2 in the following code.
The first prompt gives you a graphical view of the mob's condition throughtout the battle.
The second give you a percentage of the mob's health.
The ^w,^r,^g,^Y,^x are the colour codes used in my mud so either
change them to use for your mud or remove them.

Any comments or ideas send them to:
 Ian McCormick (aka Gothar)
 mcco0055@algonquinc.on.ca


Here is the help entry for the prompts, just include these to the end of
the entry already present.

               ^r****************^W BATTLE PROMPTS^r ****************^w
                <- Gothar's Battle Prompts ->
        %b :  Displays a graphical view of a mob's health
                 ^wEnemy: [^r+++^Y+++^g++++^x]
        %B :  Displays a percentage of a mob's health
                 ^wEnemy: ^r33%^x


In the function void bust_a_prompt( CHAR_DATA *ch )
You must add the following declarations.
    
  /* for enemy life meter */
    CHAR_DATA *victim;
    char wound[20];
    int percent;
 /* for enemy life meter */   
 
 /* at the start of the bust_a_prompt */
     ++str;
      switch( *str )
      {
         default :
            i = " "; break;
	case 'e':
	    found = FALSE;
	    doors[0] = '\0';
	    for (door = 0; door < 6; door++)
	    {
		if ((pexit = ch->in_room->exit[door]) != NULL
		&&  pexit ->u1.to_room != NULL
		&&  (can_see_room(ch,pexit->u1.to_room)
		||   (IS_AFFECTED(ch,AFF_INFRARED) 
		&&    !IS_AFFECTED(ch,AFF_BLIND)))
		&&  !IS_SET(pexit->exit_info,EX_CLOSED))
		{
		    found = TRUE;
		    strcat(doors,dir_name[door]);
		}
	    }
	    if (!found)
	 	strcat(buf,"none");
	    sprintf(buf2,"%s",doors);
	    i = buf2; break;
 	 case 'c' :

/* <SNIP>
 * the other prompt options
 *  <- Gothar 1997 ->
 */

	case 'b' :

/*    this is the graphical battle damage prompt
 *    
 *    <- Gothar 1997 ->
 */

        if ((victim = ch->fighting) != NULL)
        {
            if (victim->max_hit > 0)
                percent = victim->hit * 100 / victim->max_hit;
            else
                percent = -1;
            if (percent >= 100)
                sprintf(wound,"^wEnemy: [^r+++^Y+++^g++++^x]");
            else if (percent >= 90)
                sprintf(wound,"^wEnemy: [^r+++^Y+++^g+++ ^x]");
            else if (percent >= 80)
                sprintf(wound,"^wEnemy: [^r+++^Y+++^g++  ^x]");
            else if (percent >= 70)
                sprintf(wound,"^wEnemy: [^r+++^Y+++^g+   ^x]");
            else if (percent >= 58)
                sprintf(wound,"^wEnemy: [^r+++^Y+++    ^x]");
            else if (percent >= 45)
                sprintf(wound,"^wEnemy: [^r+++^Y++     ^x]");
            else if (percent >= 30)
                sprintf(wound,"^wEnemy: [^r+++^Y+      ^x]");
            else if (percent >= 28)
                sprintf(wound,"^wEnemy: [^r+++^x       ^x]");
            else if (percent >= 15)
                sprintf(wound,"^wEnemy: [^r++^x        ^x]");
            else if (percent >= 8)
                sprintf(wound,"^wEnemy: [^r+^x         ^x]");
            else
                sprintf(wound,"^wEnemy: [          ^x]");
            sprintf(buf2," %s",wound);
            i = buf2;
        }
        else
           i = "";         
        break;    
	case 'B' :

/*    this is the percentage battle damage prompt
 *    The prompt changes colour to show the 
 *    condition of the mob.
 *    <- Gothar 1997 ->
 */
           if ((victim = ch->fighting) != NULL)
           {
              percent = victim->hit * 100 / victim->max_hit;
              if(percent >= 65)
                 sprintf(buf2," ^wEnemy: ^g%d^x%%",percent);
              else if(percent >= 25 && percent < 65)
                 sprintf(buf2," ^wEnemy: ^Y%d^x%%",percent);
              else
                 sprintf(buf2," ^wEnemy: ^r%d^x%%",percent);
              i = buf2;
           }
           else
              i = "";         
           break;    
      }          /* end of switch */
      ++str;
 