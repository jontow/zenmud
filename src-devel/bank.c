/* Original credit goes to:

	Judson E. Knott <jek@conga.oit.unc.edu>

  (much) later ported/enhanced for ROM 2.4b6 by:

	Jonathan Towne <jontow@zenbsd.net>
	2007-05-22

*/


#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "merc.h"
#include "interp.h"

void do_balance(CHAR_DATA *ch, char *argument)
{
	char buf[MAX_STRING_LENGTH];

	if (IS_NPC(ch))
		return;

	sprintf(buf, "You have %ld silver and %ld gold coins in the bank.\n\r", ch->banksilver, ch->bankgold);
	send_to_char(buf, ch);
	return;
}

void do_deposit(CHAR_DATA *ch, char *argument)
{
	CHAR_DATA *banker;
	char currency[MAX_INPUT_LENGTH];
	char amount[MAX_INPUT_LENGTH];
	char buf[MAX_STRING_LENGTH];
	long int amnt;

	if (IS_NPC(ch))
		return;

	if (!IS_SET(ch->in_room->room_flags, ROOM_BANK)) {
		sprintf(buf, "But you are not in a bank.\n\r");
		send_to_char(buf, ch);
		return;
	}
	banker = NULL;
	for (banker = ch->in_room->people; banker; banker = banker->next_in_room) {
		if (IS_NPC(banker) && IS_SET(banker->pIndexData->act, ACT_BANKER))
			break;
	}

	if (!banker) {
		sprintf(buf, "The banker is currently not available.\n\r");
		send_to_char(buf, ch);
		return;
	}
	argument = one_argument(argument, amount);
	argument = one_argument(argument, currency);

	if (amount[0] == '\0') {
		sprintf(buf, "deposit <amount> [currency]\n\rNote: currency defaults to silver\n\r");
		send_to_char(buf, ch);
		return;
	}
	if (strlen(currency) == 0)
		strncpy(currency, "silver", sizeof(currency));

	amnt = atol(amount);

	if (!strcmp(currency, "silver")) {
		if (amnt > ch->silver) {
			sprintf(buf, "%s, you do not have %ld silver coins.", ch->name, amnt);
			do_say(banker, buf);
			return;
		}
		if ((ch->banksilver += amnt) < 0)
			ch->banksilver = INT_MAX;
		ch->silver -= amnt;
	} else if (!strcmp(currency, "gold")) {
		if (amnt > ch->gold) {
			sprintf(buf, "%s, you do not have %ld gold coins.", ch->name, amnt);
			do_say(banker, buf);
			return;
		}
		if ((ch->bankgold += amnt) < 0)
			ch->bankgold = INT_MAX;

		ch->gold -= amnt;
	} else {
		do_say(banker, "Thats an unknown currency!  We deal in silver and gold, here.");
		sprintf(buf, "amount: %s, currency: %s\n\r", amount, currency);
		send_to_char(buf, ch);
		return;
	}

	sprintf(buf, "%s, your account now contains: %ld silver and %ld gold coins,", ch->name, ch->banksilver, ch->bankgold);
	do_say(banker, buf);
	sprintf(buf, "after depositing: %ld %s coins.", amnt, currency);
	do_say(banker, buf);
	return;
}

void do_withdraw(CHAR_DATA *ch, char *argument)
{
	CHAR_DATA *banker;
	char currency[MAX_INPUT_LENGTH];
	char amount[MAX_INPUT_LENGTH];
	char buf[MAX_STRING_LENGTH];
	long int amnt;

	if (IS_NPC(ch))
		return;

	if (!IS_SET(ch->in_room->room_flags, ROOM_BANK)) {
		sprintf(buf, "But you are not in a bank.\n\r");
		send_to_char(buf, ch);
		return;
	}
	banker = NULL;
	for (banker = ch->in_room->people; banker; banker = banker->next_in_room) {
		if (IS_NPC(banker) && IS_SET(banker->pIndexData->act, ACT_BANKER))
			break;
	}

	if (!banker) {
		sprintf(buf, "The banker is currently not available.\n\r");
		send_to_char(buf, ch);
		return;
	}
	argument = one_argument(argument, amount);
	argument = one_argument(argument, currency);

	if (amount[0] == '\0') {
		sprintf(buf, "withdraw <amount> [currency]\n\rNote: currency defaults to silver.\n\r");
		send_to_char(buf, ch);
		return;
	}
	amnt = atol(amount);

	if (strlen(currency) == 0)
		strncpy(currency, "silver", sizeof(currency));

	if (!strcmp(currency, "silver")) {
		if (amnt >= (ch->banksilver + 1)) {
			sprintf(buf, "%s, you do not have %ld silver coins in the bank.", ch->name, amnt);
			do_say(banker, buf);
			return;
		}
		if ((ch->silver += amnt) < 0)
			ch->silver = INT_MAX;
		ch->banksilver -= amnt;
	}
	if (!strcmp(currency, "gold")) {
		if (amnt >= (ch->bankgold + 1)) {
			sprintf(buf, "%s, you do not have %ld gold coins in the bank.", ch->name, amnt);
			do_say(banker, buf);
			return;
		}
		if ((ch->gold += amnt) < 0)
			ch->gold = INT_MAX;
		ch->bankgold -= amnt;
	}
	sprintf(buf, "%s, your account now contains: %ld silver and %ld gold coins,", ch->name, ch->banksilver, ch->bankgold);
	do_say(banker, buf);
	sprintf(buf, "after withdrawing: %ld %s coins.", amnt, currency);
	do_say(banker, buf);
	return;
}
