2014-01-09 -- jontow@zenbsd.net

Wrote OpenBSD.dot_indent.pro file, ran OpenBSD indent(1) against all
source (*.[ch]) files.  Found a few problems, corrected most:

* db.c: #if 0'd code with "envy else version" caused indent(1) parser
		errors, removed that old unused code.
* asciimap.c: messy old code left a bunch of { } laying around, removed.
* olc_save.c: #if defined(VERBOSE)/(!VERBOSE) preprocessor directives
		surrounding a few case statements inside a switch() caused much
		confusion of parser.  Commented that block out, reran indent(1),
		uncommented block and manually stylized that block.

NOTE: indent(1) will still not run cleanly against olc_save.c
