#!/bin/sh

PKG_NAME="zenmud"
PUSH_TO="user@host.example.com:/path/"

if [ ! -z "$1" ]; then
    PUSH_TO="$1"
fi

# Generic platform name
platform="$(uname -s)-unknown-$(uname -m)"
# Platform overrides, if found
if [ -f /etc/alpine-release ]; then
    platform="alpine-$(cat /etc/alpine-release)-$(uname -m)"
elif [ -f /etc/redhat-release ]; then
    platform="redhat-$(awk '{print $4}' /etc/redhat-release)-$(uname -m)"
elif [ -f /etc/lsb-release ]; then
    distrib_id="$(awk -F= /^DISTRIB_ID=/ '{print $2}')"
    distrib_rel="$(awk -F= /^DISTRIB_RELEASE=/ '{print $2}')"
    platform="${distrib_id}-${distrib_rel}-$(uname -m)"
fi

build_name=$(basename "$(pwd)")

cd src-devel || exit 1
make || exit 1
mv rom ../ || exit 1
cd ../.. || exit 1
tar_paths=""
for include_path in area doc gods player README* rom run.sh.example; do
    tar_paths="${tar_paths} ${build_name}/${include_path}"
done

tar zcvf "${PKG_NAME}-${platform}-bin.tgz" $(echo "${tar_paths}") || exit 1
scp "${PKG_NAME}-${platform}-bin.tgz" "${PUSH_TO}" || exit 1
